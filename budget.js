var expenses = []
var budget = 0;

function addExpense() {

    if (validateExpenseForm()) {

        let title = document.getElementById("expenseTitle").value;
        let amount = parseFloat(document.getElementById("expenseAmount").value);
        let json = {};
        json.title = title;
        json.amount = amount;
        let index = expenses.findIndex(e => e.title == title)
        if (index >= 0) {
            expenses[index].title=title;
            expenses[index].amount=amount;
        } else {
            expenses.push(json);
        }
        document.getElementById("expenseTitle").value = "";
        document.getElementById("expenseAmount").value = "";
        createTable();
        calculateBudget();
    }

}

function calculateBudget() {
    let expense = 0;
    let budgetAmt = document.getElementById("budgetAmt");
    let expenseAmt = document.getElementById("expenseAmt");
    let balanceAmt = document.getElementById("balanceAmt");
    //if (expenses.length > 0) {
    for (let i = 0; i < expenses.length; i++) {
        expense = expense + expenses[i].amount;
    }
    let balance = budget - expense;

    budgetAmt.innerHTML = budget;
    expenseAmt.innerHTML = expense;
    balanceAmt.innerHTML = balance;
    //setBudget();

}

function setBudget() {
    if (validateBudgetForm()) {
        budget = parseFloat(document.getElementById("budgetAmount").value);
        calculateBudget();
    }
    document.getElementById("budgetAmount").value = "";
}


function createTable() {

    let div = document.getElementById('expense_table');
    var table = document.createElement('table');
    table.setAttribute("border", "2")

    let tr = document.createElement('tr');
    let td1 = document.createElement('td');
    let td2 = document.createElement('td');
    let td3 = document.createElement('td');

    let text1 = document.createTextNode("Expense Title");
    let text2 = document.createTextNode("Expense Amount");
    let text3 = document.createTextNode("");

    td1.appendChild(text1);
    td2.appendChild(text2);
    td3.appendChild(text3);
    tr.appendChild(td1);
    tr.appendChild(td2);
    tr.appendChild(td3);
    table.appendChild(tr);

    for (let e = 0; e < expenses.length; e++) {

        let tr = document.createElement('tr');
        let td1 = document.createElement('td');
        td1.setAttribute("class", "red-color");
        let td2 = document.createElement('td');
        td2.setAttribute("class", "red-color");
        let td3 = document.createElement('td');
        td3.setAttribute("align", "center");

        let text1 = document.createTextNode(expenses[e].title);
        let text2 = document.createTextNode(expenses[e].amount);

        let i = document.createElement('i');
        i.setAttribute("class", "fa fa-edit edit");
        i.setAttribute("onClick", "editExpense('" + expenses[e].title + "')");

        let i2 = document.createElement('i');
        i2.setAttribute("class", "fa fa-trash trash");
        i2.setAttribute("onClick", "deleteExpense('" + expenses[e].title + "')");


        td1.appendChild(text1);
        td2.appendChild(text2);
        td3.appendChild(i);
        td3.appendChild(i2);

        tr.appendChild(td1);
        tr.appendChild(td2);
        tr.appendChild(td3);
        table.appendChild(tr);

        div.innerHTML = "";
        div.appendChild(table);
    }

}

function validateBudgetForm() {
    let budget = document.getElementById("budgetAmount");
    var regex = /^\d*[.]?\d*$/;
    if (budget.value == ""|| !regex.test(budget.value)  ) {
        alert("Please enter your valid budget amount.");
        budget.focus();
        return false;
    }
    return true;
}


function validateExpenseForm() {
    var expensetit = document.getElementById("expenseTitle");
    var expenseVal = document.getElementById("expenseAmount");
    var regex = /^\d*[.]?\d*$/;
    if (expensetit.value == "") {
        alert("Please enter your expense title.");
        expensetit.focus();
        return false;
    }
    if (expenseVal.value == "" || !regex.test(expenseVal.value)) {
        alert("Please enter your valid expense amount.");
        expenseVal.focus();
        return false;
    }
    return true;
}

function deleteExpense(title) {
    let index = expenses.findIndex(e => e.title == title)
    expenses.splice(index, 1);
    createTable();
    calculateBudget();
}

function editExpense(title) {
    let expense = expenses.find(e => e.title == title)
    document.getElementById("expenseTitle").value = expense.title;
    document.getElementById("expenseAmount").value = expense.amount;
}
